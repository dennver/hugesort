TARGET = hugesort
CC = g++
CFLAGS = -g -std=c++11 -Wall
OBJECTS = $(patsubst %.cpp, %.o, $(wildcard *.cpp))
HEADERS = $(wildcard *.h)

%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@

all: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(TARGET)	
	@echo $(TARGET) compiling has been completed
	
clean:
	-rm -f *.o


