#ifndef MERGESORT_H
#define MERGESORT_H
#include <deque>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <thread>

using namespace std;

struct MyException;

// class template declaration
template<class T> class mergesort
{
public:
    ///@brief mergesort(inFileName, outFileName, maxDequeSize)
    ///@param inFileName - input filename
    ///@param outFileName - output filename
    ///@param maxDeaueSize - maximum number of deque items to have in memory at one time
	mergesort(string inFileName, string outFileName, int maxDequeSize);

private:    
    ///@brief merges two sorted deques together and returns the sorted deque
	deque<T> merge(deque<T> &left, deque<T> &right);

    ///@brief sorts the deque
    static void sort(deque<T> *d);

    ///@brief readInputFile reads from the input file and writes an output file for each deque
    ///@param inputFile - input file stream
    ///@return number of sorted temporary files written on disk
    deque<string> readInputFile(ifstream &inputFile);

    ///@brief reads an input file stream file and stores the data in a deque.
    ///@param file - input file stream
    ///@param data - deque to store read data
    ///@return bool indicating if the file has not reached EOF
	bool readFile(ifstream &file, deque<T> &data);

    ///@brief opens a file with the specified filename and writes the contents of the deque to it.
    ///@param fileName - name of the file to open and write to
    ///@param data - deque containing data
    ///@param append - mode to open file
    void writeFile(string fileName, deque<T> &data, bool append);

    ///@brief merges the sortedfiles until one file left
    string mergeFiles(deque<string> files);

	int maxDequeSize;
};


template<class T> mergesort<T>::mergesort(string inFileName, string outFileName, int maxDequeSize)
{
    deque<string> files;
    ifstream inputFile;
    inputFile.exceptions(ifstream::failbit | ifstream::badbit | ifstream::eofbit);

    this->maxDequeSize = maxDequeSize;

    //open the input file
    try {
        inputFile.open(inFileName, ios_base::binary);
    }
    catch (ifstream::failure e) {
        throw MyException("Could not open the input file!\nError "  + string(e.what()));
    }

    //read the input file
    cout << "Reading the input file" << endl;
    try {
        files = readInputFile(inputFile);
    }
    catch (exception e) {
        throw MyException("Error reading input file " + string(e.what()));
    }

    cout << "Done!" << endl;

    //close the input file
    inputFile.close();

    cout << "\nMerging files" << endl;

    remove(outFileName.c_str());
    string sortedFile;
    try {
        sortedFile = mergeFiles(files);
    }
    catch (exception e) {
        throw MyException("Could not open the input file!\nError "  + string(e.what()));
    }
    rename(sortedFile.c_str(), outFileName.c_str());
    remove(sortedFile.c_str());
    cout << "Done!" << endl;
}


template<class T> deque<T> mergesort<T>::merge(deque<T> &left, deque<T> &right){
    deque<T> result;

    //merge the left and right deque by pulling of the lesser value of the front two values
    while(left.size() > 0 || right.size() > 0) {
        if (left.size() > 0 && right.size() > 0) {
            if (left.front() <= right.front()){
                result.push_back(left.front());
                left.pop_front();
            } else {
                result.push_back(right.front());
                right.pop_front();
            }
        } else if(left.size() > 0) {
            result.push_back(left.front());
            left.pop_front();
        } else if (right.size() > 0){
            result.push_back(right.front());
            right.pop_front();
        }
    }

    return result;
}

template<class T> void mergesort<T>::sort(deque<T> *d)
{
    std::sort(d->begin(), d->end());
}

template<class T> deque<string> mergesort<T>::readInputFile(ifstream &inputFile)
{
    const int numberOfThreads = thread::hardware_concurrency();
    vector<deque<T> > data(numberOfThreads);

    vector<thread> threads(numberOfThreads);
    T temp;
    deque<string> files;

    //loop until out of input
    while (inputFile.good()) {
        threads.clear();

        for (auto j = 0; j < numberOfThreads; j++) {
            for (auto i = 0; i < maxDequeSize && inputFile.good(); i++) {

                try {
				    // � ������ ���� ��������� � ����� �����, ��� ����� �������
                    inputFile.read((char*)&temp, sizeof(T));
                }
                catch (exception e) {
                    if (inputFile.eof())
                        break;
                    else
                        throw MyException("Error reading file " + string(e.what());
                }
                data[j].push_back(temp);
            }
            //data = sort(data);
            //threads.push_back(thread([&]() {std::sort(data[j].begin(), data[j].end());}));
            threads.push_back(thread(static_cast<void (*)(std::deque<T>*)> (&mergesort<T>::sort), &data[j]));
        }

        for (auto &t: threads)
            t.join();

        for (auto k = 0; k < numberOfThreads; k++) {
            string fileName = "./";
            fileName += tmpnam(nullptr);

            writeFile(fileName, data[k], false);
            data[k].clear();
            files.push_back(fileName);
        }
        cout << ".";
    }

    cout << endl;
    return files;
}


template<class T> bool mergesort<T>::readFile(ifstream &file, deque<T> &data)
{
    T temp;

    for (auto i = 0; i < maxDequeSize && file.good(); i++) {
        file.read((char*)&temp, sizeof(T));
        if (!file)
            ;//std::cout << "error: only " << file.gcount() << " could be read\n";
        else
            data.push_back(temp);
    }

    return file.good();
}


template<class T> void mergesort<T>::writeFile(string fileName, deque<T> &data, bool append)
{
    ofstream outputFile;

    if (append)
        outputFile.open(fileName, ios_base::app | ios_base::binary);
    else
        outputFile.open(fileName, ios_base::binary);

    T tData;

    while(data.size() > 0){
        tData = data.front();
		// ��� �� ����� �� ���� ��������� � ����� �����, �������� �� �������
        outputFile.write((char*)&tData, sizeof(T));
        data.pop_front();
    }

    outputFile.close();
}

template<class T> string mergesort<T>::mergeFiles(deque<string> files)
{
    ifstream inFile1, inFile2;
    string fileName1, fileName2;
    string tempFile;
    deque<T> data1;
    deque<T> data2;
    while (files.size() >= 2) {

        bool fileGood1 = true, fileGood2 = true;

        fileName1 = files.front();
        files.pop_front();
        fileName2 = files.front();
        files.pop_front();

        //open the files
        try {
            inFile1.open(fileName1, ios_base::binary);
            inFile2.open(fileName2, ios_base::binary);
        } catch (exception e) {
            throw MyException("Could not open the open the files!\nError " + string(e.what()));
        }

        tempFile = "./";
        tempFile += tmpnam(nullptr);
        while (fileGood1 || fileGood2) {
            if(data1.size() == 0)
                fileGood1 = readFile(inFile1, data1);
            if(data2.size() == 0)
                fileGood2 = readFile(inFile2, data2);

            data1 = merge(data1, data2);

            writeFile(tempFile, data1, true);

            data1.clear();
        }

        //close and delete the unneeded files
        inFile1.close();
        inFile2.close();
        remove(fileName1.c_str());
        remove(fileName2.c_str());
        
        files.push_back(tempFile);

        cout << ".";
    }
    cout << endl;
    return files.back();
}

struct MyException : public exception
{
   MyException(string ss) : s(ss) {}
   ~MyException() throw () {}
   const char* what() const throw() { return s.c_str(); }
private:
   string s;
};


#endif
