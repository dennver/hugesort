#include <iostream>
#include <getopt.h>
//#include <gtest.h>
#include "mergesort.h"
#include <fstream>


#ifdef WINDOWS
#include <windows.h>

size_t getTotalSystemMemory()
{
    MEMORYSTATUSEX status;
    status.dwLength = sizeof(status);
    GlobalMemoryStatusEx(&status);
    return status.ullTotalPhys;
}
#endif

#ifdef LINUX
#include <unistd.h>

size_t getTotalSystemMemory()
{
    long pages = sysconf(_SC_PHYS_PAGES);
    long page_size = sysconf(_SC_PAGE_SIZE);
    return pages * page_size;
}
#endif

using namespace std;

int main(int argc, char *argv[])
{
    int opt = 0;
    string inFile = "";
    string outFile = "";
    int chunkSize = 0;

    static struct option long_options[] =
    {   {"file",   required_argument, 0, 'f'},
        {"output", required_argument, 0, 'o'},
        {"chunk",  required_argument, 0, 'c'},
        {"help",   no_argument,       0, 'h'},
        {0, 0, 0, 0}                        };

    while ((opt = getopt_long(argc, argv, "f:o:c:h", long_options, nullptr)) != -1) {
        switch(opt) {
        //----------------------------------------------------------------------------------
        // параметр -f, --file <filename> указывает имя файла
        case 'f':
        {
            inFile = optarg;
        }
            break;
            //----------------------------------------------------------------------------------------
            // -o, --output <output file name>   указывает имя выходного файла
        case 'o':
        {
            outFile = optarg;
        }
            break;
        case 'c':
        {
            chunkSize = atoi(optarg);
            if (chunkSize <= 0L) {
                cout << "Incorrect chunk size";
                return 1;
            }
        }
            break;
        case 'h':
            cout << "How to: hugesort [--file <filename>]"
                    "[--output <output file name>] [--chunk <chunk size>] [--help]" << endl;

            cout << "Options:\n"
                    "-f, --file <filename>              file to sort\n"
                    "-o, --output <output file name>    file to put sorted data in\n"
                    "-c, --chunk <max chunk size>       size of max chunk to read from input file\n"
                    "-h, --help                         help\n"
                 << endl;

            cout << "example:\n"
                    "hugesort --file ./unsorted.bin --output ./sorted.bin --chunk 320\n"
                    "hugesort -f ./unsorted.bin -o ./sorted.bin -c 1024" << endl;
            return 0;
            break;
        case 0: // long option without a short arg
            break;
        case '?':
            if (optopt == 'f')
                cout << "Option -f requires an argument." << endl;
            else if (optopt == 'o')
                cout << "Option -o requires an argument." << endl;
            else if (optopt == 'c')
                cout << "Option -c requires an argument." << endl;
            else if (isprint(optopt))
                cout << "Unknown option '" << optopt << "'." << endl;
            return 1;
        default:
            cout << "Incorrect prompt parameters" << endl;
            return 1;
        }
    }
    if (argc < 7) {
        cout << "type: hugesort -h for help";
        return 1;
    }


    try {
        mergesort<unsigned int> test2(inFile, outFile, chunkSize);
    } catch (exception e) {
        cout << e.what();
        return 1;
    }

    cout << inFile << " sorted!" << endl;

//    ifstream ofs;
//    ofs.open(outFile, std::ios_base::binary);
//    int sizetoread = 734;
//    unsigned int arr[sizetoread];
//    unsigned int comp[2];
//    ofs.read((char*)arr, sizetoread * sizeof(unsigned int));
//    for (auto i = 0; i < sizetoread;) {
//        comp[0] = arr[i++];
//        comp[1] = arr[i++];
//        if (comp[0] > comp[1]) {
//            cout << comp[0] << " > " << comp[1] << endl;
//            cout << "Achtung!" << endl;
//            return 1;
//        }
//    }


    return 0;
}



